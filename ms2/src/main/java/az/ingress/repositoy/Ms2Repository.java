package az.ingress.repositoy;

import az.ingress.domain.Ms2Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Ms2Repository extends JpaRepository<Ms2Entity,Long> {
}
