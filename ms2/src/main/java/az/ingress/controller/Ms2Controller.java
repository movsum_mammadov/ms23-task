package az.ingress.controller;

import az.ingress.sevice.Ms2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/hello")
public class Ms2Controller {

    private final Ms2Service ms2Service;

    @GetMapping
    public String getCount(){
        return ms2Service.increaseCount();
    }

}
