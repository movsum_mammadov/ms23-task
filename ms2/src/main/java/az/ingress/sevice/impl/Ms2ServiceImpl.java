package az.ingress.sevice.impl;

import az.ingress.domain.Ms2Entity;
import az.ingress.repositoy.Ms2Repository;
import az.ingress.sevice.Ms2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Ms2ServiceImpl implements Ms2Service {

    private final Ms2Repository ms2Repository;
    @Override
    public String increaseCount() {
        Ms2Entity ms2Entity = ms2Repository.findById(1L).orElseGet(
                () -> ms2Repository.save(Ms2Entity.builder().count(0).build())
        );
        ms2Entity.setCount(ms2Entity.getCount() + 1);
        return "Hello from MS2, count: %s".formatted(ms2Repository.save(ms2Entity).getCount());

    }
}
