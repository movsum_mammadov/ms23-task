package az.ingress.repositoy;

import az.ingress.domain.Ms1Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Ms1Repository extends JpaRepository<Ms1Entity,Long> {
}
