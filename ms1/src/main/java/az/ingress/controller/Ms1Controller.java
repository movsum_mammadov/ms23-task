package az.ingress.controller;

import az.ingress.sevice.Ms1Service;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/hello")
public class Ms1Controller {

    private final Ms1Service ms1Service;

    @GetMapping
    public String getCount(){
        return ms1Service.increaseCount();
    }

}
