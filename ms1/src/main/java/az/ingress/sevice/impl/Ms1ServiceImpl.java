package az.ingress.sevice.impl;

import az.ingress.domain.Ms1Entity;
import az.ingress.repositoy.Ms1Repository;
import az.ingress.sevice.Ms1Service;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Ms1ServiceImpl implements Ms1Service {

    private final Ms1Repository ms1Repository;
    @Override
    public String increaseCount() {
        Ms1Entity ms1Entity = ms1Repository.findById(1L).orElseGet(
                () -> ms1Repository.save(Ms1Entity.builder().count(0).build())
        );
        ms1Entity.setCount(ms1Entity.getCount() + 1);
        return "Hello from MS1, count: %s".formatted(ms1Repository.save(ms1Entity).getCount());

    }
}
